require([
    'jquery',
    'jira/analytics',
    'jira/util/events',
    'dndattachment/util/events/types'
], function (
    $,
    analytics,
    Events,
    EventTypes
) {

        var ISSUEACTION_ATTACH_FILES_CLASS = "issueaction-attach-file";

        var addDialogClassAndInvoke = function($el) {
            $el.addClass(ISSUEACTION_ATTACH_FILES_CLASS);
            $el.click();
        };

        var openFilePickerForPage = function() {
            var $tempFileInput = $("<input type=\"file\" multiple />");
            $tempFileInput.change(function (event) {
                Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
                    files: $tempFileInput[0].files
                });
            });
            $tempFileInput.click();
        };

        var isSubtaskLink = function($link) {
            return !! $link.data("issuekey");
        };

        // This should use skate once skate is a thing in jira-core
        var initialiseMenuAttachmentLink = function () {
            $(document).on('click', '.unified-attach-file', function (e) {
                analytics.send({ name : 'issue.dnd.attachment.opsbar.attachFiles.linkClick', data : {}});

                var $target = $(e.target);

                // If we have the old issueaction class, then do nothing
                if($target.hasClass(ISSUEACTION_ATTACH_FILES_CLASS)) {
                    return;
                }

                e.preventDefault();

                // Check if we have a Drop Zone present
                var isDropZonePresent = $(".issue-drop-zone").length;
                if(isDropZonePresent && !isSubtaskLink($target)) {
                    // If we have a drop zone available then we  create a dummy file input and open it
                    openFilePickerForPage();
                } else {
                    // If we do not, then add the class to make it behave the old way then click the target again to open the dialog
                    // This is mainly for JIRA Agile, who won't have a drop zone available to listen accept the files.
                    addDialogClassAndInvoke($target);
                }
            });
        };

        var onReady = function () {
            initialiseMenuAttachmentLink();
        };

        if ($.isReady) {
            onReady();
        } else {
            $(onReady);
        }
    });
