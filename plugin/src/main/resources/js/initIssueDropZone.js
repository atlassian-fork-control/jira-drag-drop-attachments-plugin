/*
* This is an initialization script and used to reside in a "require" block. It has been updated to a define block even though it's not a module
* to make sure it only execute once even if loaded more than once due to to https://ecosystem.atlassian.net/browse/PLUGWEB-411.
* fixes https://jira.atlassian.com/browse/JSWSERVER-14967
*/

define('dndattachment/init',[
    'jira/util/events',
    'jira/util/events/types',
    'jira/dialog/dialog-register',
    'jira/dialog/dialog',
    'underscore',
    'dndattachment/Parser',
    'jquery',
    'dndattachment/dropzones/IssueDropZone',
    'dndattachment/upload/handler',
    'dndattachment/upload/default/executor',
    'dndattachment/TemporaryAttachments',
    'dndattachment/templates',
    'dndattachment/util/Configuration',
    'dndattachment/util/FileSizeUtil'
],
    function(
        Events,
        Types,
        Dialogs,
        Dialog,
        _,
        Parser,
        $,
        IssueDropZone,
        uploadHandler,
        defaultUploadExecutor,
        Attachments,
        DnDTemplates,
        Config,
        FileSizeUtil) {
        if (!IssueDropZone.prototype.isSupportedBrowser()) {
            return;
        }

        var uploadLimit = Config.getWRM("upload-limit");
        var attachmentDropzone;

        function createDropZoneInContext($context, duiType) {
            var $fileInputList = $context.find('.field-group.file-input-list');

            if ($fileInputList.size() == 0) {
                return;
            }

            // skip if we already have dropzone there
            if ($fileInputList.find('*[duiType*="' + duiType + '"]').length > 0) {
                return;
            }

            var attachments = $fileInputList.find('input[name=filetoconvert]:checked').map(function (idx, el) {
                return Attachments.getAttachment(el.value, $(el).siblings('label').text());
            }).toArray();

            var description = $fileInputList.find('.description:last-child').html();

            // clear contents
            $fileInputList.empty();

            var $dropZone = $(DnDTemplates[duiType]({
                uploadLimit: FileSizeUtil.format(uploadLimit),
                jiraAttachmentSize: uploadLimit,
                description: description
            }));

            Parser.parse($dropZone.appendTo($fileInputList)).then(function (dropZone) {
                if (dropZone != null)
                    dropZone.loadAttachments(attachments);
            });
        }

        function createDropZoneInDialog(dialog, duiType) {
            var createDropZone = function () {
                createDropZoneInContext(dialog.$popupContent, duiType);
            };
            if (dialog.$popup && dialog.$popup.is(':visible')) {
                createDropZone();
            } else {
                dialog.onContentReady(createDropZone);
            }
        }

        function createAttachmentsDropZone($context) {
            var $attachmentModule = $context && ($context.attr("id") == "attachmentmodule") && $('.mod-content:not(.issue-drop-zone)', $context);

            if ($attachmentModule && $attachmentModule.size() > 0) {
                if ($context.find('#add-attachments-link').length == 0) {
                    // add attachment link is not there, means user don't have permissions
                    // This attachment link is hidden by this plugin and defined in JIRA
                    return;
                }

                $attachmentModule.addClass('issue-drop-zone');

                var $dropZone = $(DnDTemplates.AttachmentsDropZone({
                    uploadLimit: FileSizeUtil.format(uploadLimit),
                    jiraAttachmentSize: uploadLimit
                }));

                Parser.parse($dropZone.prependTo($attachmentModule)).then(function (dropZone) {
                    if (attachmentDropzone) {
                        attachmentDropzone.disconnectContainer();
                    }
                    attachmentDropzone = dropZone;
                    uploadHandler.setAttachmentDropZone(dropZone);
                });
            }
        }

        // Requiring Parser is enough to process all duiType declarations
        // besides that we need to install issue-drop-zone in attachFile and createIssue dialogs
        // each time when they become visible.
        // This is NOT supposed to inject drop zone everywhere.
        var onReady = function () {
            if (Dialogs.attachFile) {
                createDropZoneInDialog(Dialogs.attachFile, 'AttachFilesDropZone');
            }

            var triggerDialogs = ['create-issue-dialog', 'create-subtask-dialog', 'edit-issue-dialog'];

            var onDialogShow = function (event, $popupContent, dialog) {
                if (_.contains(triggerDialogs, $popupContent.attr('id'))) {
                    createDropZoneInDialog(dialog, 'CreateIssueDropZone');
                }
            };
            Events.bind('Dialog.show', onDialogShow);

            // just in case init file was loaded after Dialog.show event
            if (Dialog.current) {
                onDialogShow(null, Dialog.current.$popup, Dialog.current);
            }

            Events.bind(Types.NEW_CONTENT_ADDED, function (e, context, reason) {
                createDropZoneInContext(context, 'CreateIssueDropZone');

                createAttachmentsDropZone(context);
            });

            var $createIssueForm = $('form#issue-create, form#issue-edit, form.dnd-attachment-support');
            if ($createIssueForm.size() > 0) {
                createDropZoneInContext($createIssueForm, 'CreateIssueDropZone');
            }

            uploadHandler.initialize();
            uploadHandler.registerExecutor(defaultUploadExecutor);
            createAttachmentsDropZone($('#attachmentmodule'));

            var DRAGOVER_CLASS = 'issue-drop-zone-document__dragover';

            $(document).on("dragover dragenter", function () {
                $('body').addClass(DRAGOVER_CLASS);
            });

            $(document).on("dragleave drop dropHandled", function () {
                $('body').removeClass(DRAGOVER_CLASS);
            });
        };

        if ($.isReady) {
            onReady();
        } else {
            $(onReady);
        }

        // we want dataTransfer property from originalEvent
        $.event.props.push("dataTransfer");
    });

require('dndattachment/init');