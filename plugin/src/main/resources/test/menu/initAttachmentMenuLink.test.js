AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:attachment-menu-link-init"], function () {
    "use strict";

    var $ = require("jquery");

    test("Use normal dialog for subtask attach link", function (assert) {
        var $body = $("body");
        var $link = $("<a class='unified-attach-file' data-issuekey='X'></a>");
        var $dropZone = $("<div class='issue-drop-zone'></div>");

        $body.append($link);
        $body.append($dropZone);

        $link.click();

        assert.ok($link.hasClass("issueaction-attach-file"));
    });
});
