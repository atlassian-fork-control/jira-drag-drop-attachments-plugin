define('dndattachment/progressbars/TempUploadProgressBar', ['require'], function(require) {
    var UploadProgressBar = require('dndattachment/progressbars/UploadProgressBar');
    var flag = require('jira/flag');
    var $ = require('jquery');
    var DnDTemplates = require('dndattachment/templates');
    var FileSizeUtil = require('dndattachment/util/FileSizeUtil');
    var I18n = require('dndattachment/i18n');

    return UploadProgressBar.extend({
        init: function() {
            this._super.apply(this, arguments);
            this.bind("onDestroy", this.hideTooltips.bind(this));
        },

        render: function() {
            this.$node.html(DnDTemplates.TempUploadProgressBar());
            this.afterRender();
        },

        loadThumbnail: function(file) {
            if(UploadProgressBar.prototype.isImageType.call(this)) {
                this._super.apply(this, arguments);
            } else {
                $(JIRA.Templates.ViewIssue.renderThumbnailIcon({ mimetype: file.type }))
                    .appendTo(this.getThumbnailNode().addClass("upload-progress-bar__other-file-thumbnail"));
            }
        },

        showErrorMessage: function(message) {
            // show hidden error icon + text
            this.$node.find(".upload-progress-bar__error").show();
            // hide the thumbnail
            this.$node.find(".upload-progress-bar__thumbnail").hide();
            // give it the right error message in it's tipsy
            this.$node.find(".upload-progress-bar__control .error-message").attr("aria-label", message);
            this.$node.find(".temp-upload-progress-bar__container .upload-progress-bar__error-indicator").attr("title", message);
            this.$node.find(".temp-upload-progress-bar__container .upload-progress-bar__error-indicator").tooltip({
                title: 'title',
                delayIn: 0
            });
        },

        setFileName: function(fileName) {
            var $fileNameEl = this.$node.find('.upload-progress-bar__file-name');
            $fileNameEl.text(fileName);
            $fileNameEl.attr("title", fileName);
            this.$node.find('.upload-progress-bar__file-name').tooltip({
                title: 'title'
            });
            this.$node.find(".upload-progress-bar__control .file-name").attr("aria-label", fileName);
        },

        setFileSize: function(fileSize) {
            if(fileSize >= 0) {
                var title = this.$node.find('.upload-progress-bar__file-name').attr("original-title");
                var separator = title ? " | " : "";
                this.$node.find('.upload-progress-bar__file-name')
                    .attr("title", title + separator + FileSizeUtil.format(fileSize));
            }
        },

        setFinished: function() {
            this.$node.addClass('upload-progress-bar__upload-finished');
            this.$node.find(".temp-upload-progress-bar__uploading").removeClass("temp-upload-progress-bar__uploading");
            this.$node.find('.upload-progress-bar__progress-bar').addClass("upload-progress-bar__progress-bar-fade");
            this.$node.find(".upload-progress-bar__control .button-text").attr("aria-label", I18n("dnd.attachment.upload.remove"));
            this.getThumbnailNode().removeClass("upload-progress-bar__thumbnail-uploading");
            this.finished = true;
            this.trigger("onFinished");
        },

        setFailed: function() {
            this.$node.addClass('UploadProgressBar_uploadFailed');
            this.$node.find(".temp-upload-progress-bar__uploading").removeClass("temp-upload-progress-bar__uploading");
            this.$node.find('.upload-progress-bar__progress-bar').hide();
            this.finished = true;
            this.failed = true;
            this.trigger("onFailed");
        },

        hideTooltips: function() {
            this.$node.find('.upload-progress-bar__file-name').tooltip("hide");
            this.$node.find(".temp-upload-progress-bar__container .upload-progress-bar__error-indicator").tooltip("hide");
        }
    });
});
