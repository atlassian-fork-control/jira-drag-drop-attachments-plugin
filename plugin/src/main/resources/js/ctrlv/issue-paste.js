define("dndattachment/ctrlv/issue-paste", ['require', 'exports'], function(require, exports) {
    "use strict";

    var $ = require('jquery');
    var trace = require('dndattachment/ctrlv/trace');
    var tracker = require('dndattachment/ctrlv/tracking');
    var utility = require('dndattachment/ctrlv/utility');
    var html5 = require('dndattachment/ctrlv/html5');
    var base64decode = require('dndattachment/ctrlv/base64decode');
    var Dialog = require('jira/dialog/dialog');
    var Events = require('jira/util/events');
    var EventTypes = require('dndattachment/util/events/types');

    /** Ze State Machine **/

    // Event types
    var EVENT_WINDOW_KEYDOWN = 0;
    var EVENT_WINDOW_PASTE = 1;

    var EVENT_FILE_LOADED = 2;
    var EVENT_FILE_LOAD_ERROR = 3;

    var EVENT_IMAGE_LOADED = 4;
    var EVENT_IMAGE_LOAD_ERROR = 5;

    var EVENT_DIALOG_LOADED = 6;
    var EVENT_DIALOG_CLOSED = 7;
    var EVENT_DIALOG_CANCELED = 8;

    var EVENT_TIMEOUT = 9;
    /**
     * Stub function, does nothing, in future may wrap states in some default behavior
     * @param {Function} state
     * @returns {State}
     * @constructor
     */
    function State(stateName, state) {
        state.stateName = stateName;
        var result = function() {
            var handler = state.apply(null, arguments);
            handler.stateName = stateName;
            return handler;
        };
        result.stateName = stateName;

        return result;
    }

    /**
     * Wraps given state, so that it will receive EVENT_TIMEOUT event after given timeout
     * @param {Function} state
     * @param {Number} timeout in milliseconds
     * @returns {WeakState}
     * @constructor
     */
    function WeakState(stateName, state, timeoutDelay) {
        return State(stateName, function() {
            var timeout = setTimeout(function() {
                triggerEvent(EVENT_TIMEOUT);
            }, timeoutDelay);

            var handler = state.apply(null, arguments);
            var result = function(eventType, eventObject) {
                var result = handler(eventType, eventObject);
                // we will change state, so clear timeout
                if(result) {
                    clearTimeout(timeout);
                    return result;
                }
            };
            result.stateName = stateName;

            return result;
        });
    }

    /**
     * Idle state, initial
     */
    var StateIdle = State("idle", function() {

        function isValidTarget(event) {
            // - don't play with catching paste events on any input elements other than wiki-textfield
            // - ignore content editable, because it will rather handle paste on its own
            // - summary field is an exception, because it is focused by default in create issue dialog
            if($(event.target).is(':input:not(.wiki-textfield, #summary)') || utility.isContentEditable(event.target)) {
                return;
            }

            return true;
        }


        return function(eventType, eventObject) {
            if(eventType == EVENT_WINDOW_KEYDOWN && utility.isKeyPasteEvent(eventObject)) {
                // in chrome we rely 100% on onPaste event, in Safari we don't want to make any attempts on catching clipboard data
                if (utility.browserIsNativePaste() || !isValidTarget(eventObject) || !utility.browserIsSupported()) {
                    return;
                }

                tracker.trigger("attach.screenshot.html5.catchClipboard");

                return new StateCatchClipboard(eventObject);
            }

            if(eventType == EVENT_WINDOW_PASTE) {
                if(utility.isImagePasteEvent(eventObject)) {
                    tracker.trigger("attach.screenshot.html5.handlePaste");

                    if(!isValidTarget(eventObject)) {
                        trace("jira/attach-images-plugin/pasteIgnored");
                        return;
                    }

                    /** Prevent paste now so after pressing cancel in dialog selected fragment in textarea won't be removed */
                    if($(eventObject.target).is(':input.wiki-textfield')) {
                        eventObject.preventDefault();
                    }

                    return new StateFileLoading(eventObject);
                } else {
                    trace("jira/attach-images-plugin/pasteIgnoredNotImage");
                }
            }
        }
    });

    /**
     * Catch clipboard contents after CTRL+V
     * @param {Event} keyPasteEvent
     */
    var StateCatchClipboard = WeakState("catchClipboard", function(keyPasteEvent) {
        // we will switch focus from currently active element, but we want to restore it once we are done with this state
        var activeElement = document.activeElement;

        // preserve initial selection information for later use
        keyPasteEvent.selectionStart = keyPasteEvent.target.selectionStart;
        keyPasteEvent.selectionEnd = keyPasteEvent.target.selectionEnd;

        var $contentEditable = $('<div contenteditable="true" class="attach-screenshot-paste-catcher"></div>').appendTo('body');

        // focus on content editable, so the paste event will go into this element and we will get content from it (possible an image)
        $contentEditable.focus();

        // we rely on :focusable or :aui-focusable selector from AUI, we don't want to refocus on non focusable elements, because
        // it may cause unnecessary scroll
        // also focus only on input input elements
        if($(activeElement).is(':focusable:input,:aui-focusable:input')) {
            // this quirk is required for FF, otherwise it will bug cursor
            setTimeout(function() {
                activeElement.focus();
            });
        }

        // TODO refactor this, so we won't need to call this each time we want to leave this state
        function cleanUp() {
            $contentEditable.remove();
        }

        return function(eventType, eventObject) {
            if(eventType == EVENT_WINDOW_PASTE) {
                if(utility.isImagePasteEvent(eventObject)) {
                    cleanUp();

                    eventObject.target = activeElement;
                    return new StateFileLoading(eventObject);
                }

                if(utility.isHtmlImagePasteEvent(eventObject)) {
                    cleanUp();

                    eventObject.target = activeElement;
                    return new StateImageLoading(eventObject);
                }

                if(utility.isTextPasteEvent(eventObject)) {
                    cleanUp();

                    var text1 = utility.getTextPasteContent(eventObject);
                    utility.insertToInput(text1, keyPasteEvent.target, keyPasteEvent.selectionStart, keyPasteEvent.selectionEnd);

                    // This is required for consistent behavior, IE requires preventDefault, while others don't
                    eventObject.preventDefault();

                    return new StateIdle();
                }
            }

            if(eventType == EVENT_TIMEOUT) {
                cleanUp();

                var $img = $contentEditable.find('>img');
                if($img.is(':only-child')) {
                    var imgSrc = $img.attr("src");
                    if (imgSrc.toLowerCase().indexOf("http") === 0) {
                        // we could use canvas and toDataURL here, but it is not allowed by browser:
                        // https://html.spec.whatwg.org/multipage/scripting.html#dom-canvas-todataurl
                        return StateIdle();
                    }
                    else {
                        var array = base64decode.decodeBase64DataUri(imgSrc);
                        var blob = new Blob([array], {type: "image/png"});

                        return new StateAttachImage(blob, keyPasteEvent);
                    }
                } else {
                    // there was no image paste, so we need to reinsert pasted text into text input
                    var text2 = utility.getTextContent($contentEditable[0]);
                    utility.insertToInput(text2, keyPasteEvent.target, keyPasteEvent.selectionStart, keyPasteEvent.selectionEnd);

                    return StateIdle();                 
                }
            }
        }
    });

    /**
     * Load File from image, this state is not weak, because we expect file loading to take some time
     * @param {Event} pasteEvent that contains text/html in clipboardData
     */
    var StateImageLoading = State("imageLoading", function(pasteEvent) {
        var imageSrc = utility.getHtmlImagePaste(pasteEvent);
        if(!imageSrc) {
            triggerEvent(EVENT_IMAGE_LOAD_ERROR);
        } else {
            utility.loadImage(imageSrc).then(function(image) {
                triggerEvent(EVENT_IMAGE_LOADED, image);
            }, triggerEvent.bind(null, EVENT_IMAGE_LOAD_ERROR));
        }

        return function(eventType, eventObject) {
            if(eventType == EVENT_IMAGE_LOADED) {
                var file = utility.convertImageToBlob(eventObject);

                if(file) {
                    return new StateAttachImage(file, pasteEvent);
                } else {
                    return new StateIdle();
                }
            }

            if(eventType == EVENT_TIMEOUT || eventType == EVENT_IMAGE_LOAD_ERROR) {
                return new StateIdle();
            }
        }
    });

    /**
     * Handle paste event, get file open attach screenshot dialog
     * @param {Event} pasteEvent
     */
    var StateFileLoading = WeakState("fileLoading", function(pasteEvent) {
        html5.getFileFromEvent(pasteEvent).done(function (file) {
            triggerEvent(EVENT_FILE_LOADED, file);
        }).fail(function () {
            triggerEvent(EVENT_FILE_LOAD_ERROR);
        });

        return function(eventType, eventObject) {
            if (eventType == EVENT_FILE_LOADED) {
                return new StateAttachImage(eventObject, pasteEvent);
            } else if (eventType == EVENT_FILE_LOAD_ERROR || eventType == EVENT_TIMEOUT) {
                return new StateIdle();
            }
        }
    }, 1000);

    var StateAttachImage = State("attachImage", function(file, pasteEvent) {
        var pasteTarget =  pasteEvent.target;
        var screenshotName = utility.generateFileName() + ".png";

        var pastedImage = utility.convertBlobToImage(file, screenshotName);

        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [pastedImage],
            // We want the attachment executor to know if a wiki textfield is in focus
            // so that the executor can then decide whether it wants to perform its action.
            isWikiTextfieldFocused: utility.isWikiTextfield($(pasteTarget)),
            wikiTextfield: pasteTarget,
            // Tell the dropzone whether the files came from a paste or from an actual drag and drop
            isPaste: true,
            successCallback: function() {
                triggerEvent(EVENT_DIALOG_LOADED)
            }
        });

        return function(eventType, eventObject) {
            return new StateIdle();
        };
    });

    /**
     * Current state, which will receive next event, initilized with {StateIdle}
     * @type {State}
     */
    var currentState = new StateIdle();

    /**
     * Trigger current state with given parameters
     * @param {Number} eventType
     * @param {Object} eventObject
     */
    function triggerEvent(eventType, eventObject) {
        // some states may call this function during initialization, therefore there is a need to queue such events
        // in order to avoid nested executions
        if(triggerEvent.eventQueue) {
            triggerEvent.eventQueue.push({ type: eventType, object: eventObject });
            return;
        }

        triggerEvent.eventQueue = [{ type: eventType, object: eventObject }];

        while(triggerEvent.eventQueue.length > 0) {
            var event = triggerEvent.eventQueue.splice(0, 1)[0];
            var newState = currentState(event.type, event.object);
            if(newState) {
                currentState = newState;
            }
        }

        delete triggerEvent.eventQueue;
    }

    /**
     * Required for tests
     */
    exports._getStateMap = function() {
        return {
            events: {
                EVENT_WINDOW_KEYDOWN: EVENT_WINDOW_KEYDOWN,
                EVENT_WINDOW_PASTE: EVENT_WINDOW_PASTE,

                EVENT_FILE_LOADED: EVENT_FILE_LOADED,
                EVENT_FILE_LOAD_ERROR: EVENT_FILE_LOAD_ERROR,

                EVENT_IMAGE_LOADED: EVENT_IMAGE_LOADED,
                EVENT_IMAGE_LOAD_ERROR: EVENT_IMAGE_LOAD_ERROR,

                EVENT_DIALOG_LOADED: EVENT_DIALOG_LOADED,
                EVENT_DIALOG_CLOSED: EVENT_DIALOG_CLOSED,
                EVENT_DIALOG_CANCELED: EVENT_DIALOG_CANCELED,

                EVENT_TIMEOUT: EVENT_TIMEOUT
            },
            states: {
                StateIdle: StateIdle,
                StateCatchClipboard: StateCatchClipboard,
                StateImageLoading: StateImageLoading,
                StateFileLoading: StateFileLoading,
                StateAttachImage: StateAttachImage
            }
        }
    };

    exports.initIssuePaste = function () {
        // handle CTRL+V on wiki textfields
        $(window).on('keydown', function (event) {
            triggerEvent(EVENT_WINDOW_KEYDOWN, event);
        });
        $(window).on("paste", function (event) {
            triggerEvent(EVENT_WINDOW_PASTE, utility.normalizePasteEvent(event));
        });
        Events.bind("Dialog.hide", function (event, $popup, reason) {
            if (reason) {
                triggerEvent(EVENT_DIALOG_CANCELED, $popup);
            } else {
                triggerEvent(EVENT_DIALOG_CLOSED, $popup);
            }
        });
    };
});
