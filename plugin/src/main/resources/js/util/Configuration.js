define("dndattachment/util/Configuration", ['require'], function(require) {
    var wrmData = require('wrm/data');
    var dataKey = "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:dnd-issue-drop-zone.";

    function getWRMData(key) {
        return wrmData.claim(key);
    }

    return {
        getWRM: function(key) {
            return getWRMData(dataKey + key);
        }
    }
});
