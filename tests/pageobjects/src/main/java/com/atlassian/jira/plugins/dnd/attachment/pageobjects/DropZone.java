package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.plugins.dnd.attachment.util.TestUtil.readResource;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@ParametersAreNonnullByDefault
public abstract class DropZone
{
    public static String DRAGOVER_CLASS = "issue-drop-zone-document__dragover";

    @Inject
    protected TraceContext traceContext;

    public abstract PageElement getDropZoneElement();

    public void dropImage(final String fileName)
    {
        final Tracer tracer = traceContext.checkpoint();
        dropImage(fileName, tracer);
    }

    public void dropImage(final String fileName, @Nullable final Tracer tracer)
    {
        getDropZoneElement().javascript().execute(readResource("js/test-util.js"), fileName);
        if (tracer != null)
        {
            traceContext.waitFor(tracer, "jira.issue.dnd.uploaded");
        }
    }

    public boolean isVisible()
    {
        return getDropZoneElement().timed().isVisible().byDefaultTimeout();
    }

    public boolean isAvailable()
    {
        return getDropZoneElement().timed().isPresent().byDefaultTimeout();
    }

    public abstract By getProgressBarLocator();

    public abstract PageElement getContextElement();

    private List<PageElement> getFiles()
    {
        return getContextElement().findAll(getProgressBarLocator());
    }

    public List<String> getFileNames()
    {
        return getFiles().stream()
                .map(fileElement -> fileElement.find(By.className("upload-progress-bar__file-name")))
                .map(PageElement::getText)
                .collect(toList());
    }

    public UploadProgressBar getProgressBar(final String fileName)
    {
        return getFiles().stream()
                .filter(anElement ->
                        fileName.equals(anElement.find(By.className("upload-progress-bar__file-name")).getText()))
                .findFirst()
                .map(UploadProgressBar::new)
                .orElseThrow(() ->
                        new NoSuchElementException(format("Unable to find progress bar for file name: %s", fileName)));
    }

    public List<UploadProgressBar> getProgressBars()
    {
        return getFiles().stream().map(UploadProgressBar::new).collect(toList());
    }
}
