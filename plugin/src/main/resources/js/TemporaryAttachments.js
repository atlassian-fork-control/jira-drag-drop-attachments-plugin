define('dndattachment/TemporaryAttachments', [], function() {
    var attachments = { };

    return {
        createAttachment: function(id, name, type, file) {
            return {
                id: id,
                name: name,
                type: type,
                file: file
            };
        },

        getAttachment: function(id, name) {
            return attachments[id] || this.createAttachment(id, name);
        },

        putAttachment: function(id, file, alterations) {
            alterations = alterations || {};
            return attachments[id] = this.createAttachment(id, alterations.name || file.name, alterations.type || file.type, file);
        }
    }
});