package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.plugins.jstestrunner.runner.JSTestRunner;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.io.File;

import static com.atlassian.plugins.jstestrunner.runner.JSTestRunner.plugin;
import static org.hamcrest.Matchers.is;

@WebTest({ Category.WEBDRIVER_TEST, Category.QUNIT })
public class QUnitTest extends BaseJiraWebTest
{
    @Inject
    private PageElementFinder elementFinder;

    private final File outdir;

    public QUnitTest() {
        String location = System.getProperty("jira.qunit.testoutput.location");

        if (StringUtils.isEmpty(location))
        {
            System.err.println("Writing result XML to tmp, jira.qunit.testoutput.location not defined");
            location = System.getProperty("java.io.tmpdir");
        }

        outdir = new File(location);
    }

    @After
    public void tearDown()
    {
        jira.gotoHomePage();
        final PageElement resetLink = elementFinder.find(By.cssSelector("#reset-lnk"));
        if(resetLink.isPresent())
        {
            resetLink.click();
        }
        else
        {
            System.err.println("Locale was not reset, couldn't find link");
        }
    }

    @Test
    public void runJustOurTest() throws Exception {
        new JSTestRunner(outdir, pageBinder).run(
                plugin((Matcher<String>) is("com.atlassian.jira.plugins.jira-dnd-attachment-plugin"))
        );
    }
}
