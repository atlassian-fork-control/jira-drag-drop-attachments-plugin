define("dndattachment/ctrlv/html5", ['require', 'exports'], function(require, exports) {
    "use strict";

    var formatter = require('jira/util/formatter');
    var wrmContextPath = require('wrm/context-path');
    var Deferred = require('jira/jquery/deferred');
    var $ = require('jquery');
    var _ = require('underscore');
    var base64decode = require('dndattachment/ctrlv/base64decode');
    var utility = require('dndattachment/ctrlv/utility');
    var Dialog = require('jira/dialog/dialog');
    var DialogRegister = require('jira/dialog/dialog-register');
    var SmartAjax = require('jira/ajs/ajax/smart-ajax');
    var InlineAttach = require('jira/attachment/inline-attach');

    /**
     * Try to get File Blob from event object, function is asynchronous since on some browsers we may want to
     * implement an workaround for clipboardData.items/files absence
     *
     * @param event JQuery Event object
     * @returns {$.Deferred}
     */
    exports.getFileFromEvent = function (event) {
        var deferred = new Deferred();

        if (utility.isImagePasteEvent(event)) {
            // look for image content in items and files, check mimetype and return the first one
            deferred.resolve(
                _(event.clipboardData.items)
                    .filter(function (item) {
                        return item.type.indexOf("image") !== -1;
                    }).map(function (item) {
                        return item.getAsFile();
                    })[0] ||
                    // according to bugzilla in future Mozilla will support this via files:
                    // https://bugzilla.mozilla.org/show_bug.cgi?id=891247
                _(event.clipboardData.files)
                    .filter(function (item) {
                        return item.type.indexOf("image") !== -1;
                    })[0]);
        } else {
            deferred.reject();
        }

        return deferred.promise();
    };

    //exports.screenshotPasteHandler = {
    exports.REQUEST_TIMEOUT_MILLIS = 5 * 60 * 1000;

    exports.screenshotFileUpload = {};
    exports.screenshotFileUploadUri = null;

    exports.$document = undefined;
    exports.$window = undefined;
    exports.$container = undefined;
    exports.$fakeInput = undefined;

    exports.uploadError = false;
    exports.uploadErrorMsg = "";

    exports.progressView = {
        hidden: false,
        progress: 0,
        old: 0,
        progressBarContainer: undefined,
        progressEl: undefined,
        container: undefined,
        options: undefined,
        staticProgress: undefined,

        initProgress: function () {
            this.container = this.buildContainer();
            this.progressEl = this.buildProgress();
            this.container.append(this.progressEl);

            this.options = {showPercentage: false, height: "5px"};

            this.progressBarContainer = $("#attach-screenshot-progress-container");
            this.progressBarContainer.empty();
            this.progressBarContainer.append(this.container);

            this.staticProgress = this.container;
            this.hidden = true;
        },

        finish: function () {
            this.value(100);
        },

        progressHandler: function (event) {
            var percentage = Math.round(event.loaded * 100 / event.total);
            this.value(percentage);
        },

        value: function (value) {
            if (value > 100) {
                value = 100;
            } else if (value < 0) {
                value = 0;
            }

            if (this.hidden) {
                this.progressEl.show();
                this.hidden = false;
            }

            if (this.old !== value) {
                this.progressEl.progressBar(value, this.options);
                if (value >= 100) {
                    this.progressEl.fadeOut();
                }
                this.old = value;
            }
        },

        buildContainer: function () {
            return $("<div>").addClass("file-progress");
        },

        buildProgress: function () {
            return $("<div>").attr("id", "attach-screenshot-upload-progress");
        }
    };

    exports.dialogView = {
        pasteCatcher: {},
        presenter: undefined,

        getMaxSize: function () {
            return $("#attach-max-size").text();
        },

        getFileSize: function () {
            if ($.isPlainObject(this.presenter.screenshotFileUpload) && _.isEmpty(this.presenter.screenshotFileUpload)) {
                return 0;
            }
            return this.presenter.screenshotFileUpload.size || this.presenter.screenshotFileUpload.byteLength || this.presenter.screenshotFileUpload.length;
        },

        cleanGeneralErrors: function () {
            $("#error-attach-screenshot-image").closest(".field-group").remove();
        },

        cleanFileErrors: function () {
            $("#error-attach-screenshot-filename").remove();
        },

        displayErrors: function (errors) {
            if ("compatibility" in errors) {
                utility.showErrorMsg("", errors["compatibility"]);
            }
            if ("fileName" in errors) {
                utility.showErrorMsg("", errors["fileName"]);
            }
            if ("fileUpload" in errors) {
                utility.showErrorMsg("", errors["fileUpload"]);
            }
        },

        appendBlobImage: function (blob) {
            // and use a URL or webkitURL (whichever is available to the browser)
            // to create a temporary URL to the object
            var URLObj = window.URL || window.webkitURL;
            var source = URLObj.createObjectURL(blob);
            this.presenter.screenshotFileUpload = blob;
            this.createImage(source);
        },

        /**
         * Creates and shows image
         * Supports:
         *  Chrome binary data from Webkit
         *  base64 encoded data from FireFox
         * @param uri image data
         */
        createImage: function (uri) {
            var pastedImage = new Image();
            pastedImage.onload = function () {
                // You now have the image!
            };
            pastedImage.src = uri;

            this.presenter.screenshotToUpload = pastedImage;

            // Appending image to document
            var jqueryImage = $(pastedImage);
            jqueryImage.addClass("attach-screenshot-pasted-image");
            var screenshotContainer = $("#attach-screenshot-image-container");
            screenshotContainer.empty();
            screenshotContainer.append(jqueryImage);

            this.presenter.$fakeInput.focus();

            // handle async upload
            // IE11 quirk: if this is called within onPaste handler, browser will throw "Access denied" on any XHR
            _.defer(function () {
                exports.imageCreatedHandler();
            });

        },

        /**
         * Parse the input in the paste catcher element
         */
        checkInput: function () {
            var image,
              node = exports.dialogView.pasteCatcher.childNodes[0];

            if (node) {
                // If the user pastes an image, the src attribute
                // will represent the image as a base64 encoded string.
                if ("IMG" === node.tagName) {
                    // does not start with data, try to get contents via canvas
                    if (node.src.indexOf("data:") === 0) {
                        image = node.src;
                    } else {
                        // we could use canvas and toDataURL here, but it is not allowed by browser:
                        // https://html.spec.whatwg.org/multipage/scripting.html#dom-canvas-todataurl
                    }
                }

                // Clear the inner html to make sure we're always getting the latest inserted content.
                exports.dialogView.pasteCatcher.innerHTML = "";
            }

            if (!image) {
                exports.$fakeInput.focus();
            }

            return image;
        },

        /**
         * onPaste handler, Either uses webkits clipboardData object on the paste event, or interprets
         * the data uri that has been embedded in the page by firefox.
         */
        onPaste: function (event) {
            exports.dialogView.cleanFileErrors();
            exports.dialogView.cleanGeneralErrors();

            // TODO: ensure target is not text fields - this prevents pasting text in text fields

            // We need to check if event contains image content we can use
            if (utility.isImagePasteEvent(event)) {
                // Get the items from the clipboard
                exports.getFileFromEvent(event).then(function (file) {
                    this.appendBlobImage(file);
                }.bind(this));
                // If we can't handle clipboard data directly (Firefox),
                // we need to read what was pasted from the contenteditable element
            } else {
                setTimeout(function () {
                    var image = this.checkInput();

                    if (image) {
                        // Firefox image is base64 encoded - decoding while setting up the data
                        exports.screenshotFileUpload = base64decode.decodeBase64DataUri(image);
                        exports.dialogView.createImage(image);
                    }

                }.bind(this), 0);
            }
        },

        getFakeInput: function () {
            return $("#attach-screenshot-fake-input");
        },

        getContainer: function () {
            return $("#attach-screenshot-image-container");
        },

        getIssueKey: function () {
            return $("input[name='id']").val();
        },

        getDocument: function () {
            return $(document);
        },

        getWindow: function () {
            return $(window);
        },

        getFileNameInput: function () {
            return $("#attachscreenshotname");
        },

        hasPngExtension: function (str) {
            var pattern = /\.png$/i; // REGEX: Ends with ".png". Case insensitive
            return pattern.test(str);
        },

        setFileToConvert: function (value) {
            $("input[name='filetoconvert']").val(value);
        },

        /**
         * Pasting into a content-editable element is the most cross-browser HTML5 approach.
         */
        buildPasteCatcher: function () {
            if (!document.getElementById("attach-screenshot-form")) return;
            var catcher = document.createElement("div");
            catcher.setAttribute("contenteditable", "true");
            catcher.style.width = 0;
            catcher.style.height = 0;
            /* make sure the catcher is rendered outside the browser view to prevent vertical scrollbars */
            catcher.style.position = 'absolute';
            catcher.style.top = '-5000px';
            document.getElementById("attach-screenshot-form").appendChild(catcher);
            return catcher;
        },

        _getFormSubmits: function () {
            return $("#attach-screenshot-form").find("button.aui-button");
        },

        disable: function () {
            this._getFormSubmits().attr("disabled", "disabled");
            return this;
        },
        enable: function () {
            this._getFormSubmits().removeAttr("disabled");
            return this;
        },
        isEnabled: function () {
            return this.isVisible() && !this._getFormSubmits().attr("disabled");
        },

        isVisible: function () {
            return $("#attach-screenshot-form").length > 0;
        },

        initDialog: function (presenter) {
            this.pasteCatcher = {};
            this.presenter = presenter;
            this.pasteCatcher = this.buildPasteCatcher();
        }
    };

    exports.initScreenshotPasteHandler = function () {
        var dialogView = exports.dialogView;

        exports.screenshotFileUpload = {};
        exports.resetUploadErrors();

        exports.dialogView.initDialog(exports);

        // Caching elements needed to fix tabbing
        exports.$document = dialogView.getDocument();
        exports.$window = dialogView.getWindow();
        exports.$container = dialogView.getContainer();
        exports.$fakeInput = dialogView.getFakeInput();

        // Event Handlers
        exports.bindOnce(exports.$container, "click", exports.setFocusOnClickHandler);
        exports.bindOnce(exports.$fakeInput, "focus", exports.showFocusOnFieldHandler);
        exports.bindOnce(exports.$fakeInput, "blur", exports.hideFocusOnFieldHandler);
        exports.bindOnce(exports.$fakeInput, "keydown", exports.keyDownHandler);

        if (!utility.browserIsSupported()) {
            dialogView.displayErrors({"compatibility": formatter.I18n.getText(
                "attach.screenshot.browser.not.supported",
                "<a href=\"//www.google.com/chrome/browser/\">",
                "</a>",
                "<a href=\"//www.mozilla.org/firefox/\">",
                "</a>"
            )});
        } else {
            exports.bindOnce(exports.$window, "paste", exports.pasteHandler);
        }
    };

    exports.resetUploadErrors = function () {
        exports.uploadError = false;
        exports.uploadErrorMsg = undefined;
    };

    exports.setUploadError = function (errorMsg) {
        exports.uploadError = true;
        exports.uploadErrorMsg = errorMsg;
    };

    /**
     * Ensures only one event is bound to the element
     * @param jqueryEl
     * @param eventName
     * @param handler
     */
    exports.bindOnce = function (jqueryEl, eventName, handler) {
        jqueryEl.unbind(eventName, handler);
        jqueryEl.bind(eventName, handler);
    };

    exports.showFocusOnFieldHandler = function () {
        exports.$container.addClass("focus");
    };

    exports.hideFocusOnFieldHandler = function () {
        exports.$container.addClass("focus");
    };

    exports.setFocusOnClickHandler = function () {
        exports.$fakeInput.focus();
    };

    exports.pasteHandler = function (event) {
        if (!exports.dialogView.isEnabled()) {
            return;
        }
        event = utility.normalizePasteEvent(event);

        exports.dialogView.onPaste(event);
    };

    exports.polyPasteHandler = function (event, imageUri, imagePreviewUri) {
        if (!exports.dialogView.isEnabled()) {
            return;
        }

        exports.screenshotFileUpload = {length: -1};
        exports.screenshotFileUploadUri = imageUri;
        exports.dialogView.createImage(imagePreviewUri);
    };

    exports.keyDownHandler = function (event) {
        if (utility.isKeyPasteEvent(event)) {
            if (exports.dialogView.pasteCatcher.focus) {
                exports.dialogView.pasteCatcher.focus();
            }
        }
    };

    exports.imageCreatedHandler = function () {
        exports.doAjaxUpload(this.dialogView.getIssueKey(), this.dialogView.getFileNameInput().val());
    };

    exports.getMimeType = function () {
        return "image/png";
    };

    exports.createData = function () {
        return exports.screenshotFileUpload;
    };

    // TODO function is not called delete?
    exports.clipboardDataIsEmpty = function (event) {
        // Internet Explorer will fire paste event for anything but image content, therefore we can assume that such event is not empty
        if (window.clipboardData != null) {
            return false;
        }

        return !(event && event.clipboardData && event.clipboardData.types && event.clipboardData.types.length > 0);
    };

    exports.validateFileSize = function (errors, fileSize, maxSize) {
        var fileSize = fileSize || exports.dialogView.getFileSize();
        var maxSize = maxSize || exports.dialogView.getMaxSize();
        if (fileSize > maxSize) {
            //TODO: hack, using IninleAttach module, check if this a blessed way to go
            var sizes = InlineAttach.Text.fileSize(maxSize, fileSize);
            errors["fileUpload"] = formatter.I18n.getText("attach.screenshot.error.upload.too.big", sizes[1], sizes[0]);
        }
    };

    exports.validateFormData = function (fileUpload, fileName) {
        var errors = {};

        if ($.isPlainObject(fileUpload) && _.isEmpty(fileUpload)) {
            errors["fileUpload"] = formatter.I18n.getText("attach.screenshot.specify.image");
        }
        exports.validateFileSize(errors);
        if (exports.uploadError) {
            errors["fileUpload"] = formatter.I18n.getText("attach.screenshot.cant.attach.upload.failed");
        }

        if ("" == fileName) {
            errors["fileName"] = formatter.I18n.getText("attach.screenshot.specify.file.name");
        }

        if (!utility.isValidFileName(fileName)) {
            errors["fileName"] = formatter.I18n.getText("attach.screenshot.error.illegal.file.name");
        }
        return errors;
    };

    exports.doAjaxUpload = function (issueKey, fileName) {
        exports.dialogView.disable();

        var errors = {};
        exports.validateFileSize(errors);
        if (!_.isEmpty(errors)) {
            exports.dialogView.displayErrors(errors);
            exports.dialogView.enable();

            // Prevent form from uploading
            return;
        }

        var data = exports.createData(),
          mimeType = exports.getMimeType(),
          secureToken = $("#attach-screenshot-form").data("attach-secure-token"),
          formToken = $("#attach-screenshot-form").find("input[name='formToken']").attr('value');

        var requestUrl = wrmContextPath() + "/rest/internal/1.0/AttachTemporaryFile?" +
          "size=" + exports.dialogView.getFileSize() + "&" +
          "filename=" + encodeURIComponent(fileName) + "&" +
          "atl_token=" + encodeURIComponent(atl_token()) + "&" +
          "issueId=" + encodeURIComponent(this.dialogView.getIssueKey()) +
          (secureToken ? ("&secureToken=" + encodeURIComponent(secureToken)) : "") +
          (formToken ? ("&formToken=" + encodeURIComponent(formToken)) : "");

        exports.resetUploadErrors();
        exports.executeAjaxUpload(data, requestUrl, mimeType).progress(function (val) {
            if (val == 'init') {
                exports.progressView.initProgress();
            }
        }).done(function (val) {
            if (typeof val == "string") {
                val = JSON.parse(val);
            }

            exports.dialogView.setFileToConvert(val.id);
        }).fail(function (jqXHR, textStatus, msg, smartAjaxResult) {
            var errMsg;
            if (smartAjaxResult.statusText == "abort") {
                errMsg = formatter.I18n.getText("attach.screenshot.request.aborted");
            } else if (smartAjaxResult.hasData) {
                var data = JSON.parse(smartAjaxResult.data);

                if (data.errorMessage) {
                    errMsg = data.errorMessage;
                } else {
                    errMsg = SmartAjax.buildSimpleErrorContent(smartAjaxResult, {alert: false});
                }
            }
            exports.setUploadError(errMsg);
            exports.dialogView.displayErrors({"fileUpload": errMsg});
        }).always(function () {
            exports.dialogView.enable();
            // Progress Listener cleanup
            exports.progressView.finish();
        }).progress(function (val) {
            if (val != 'init') {
                exports.progressView.progressHandler(val);
            }
        });
    };

    exports.executeAjaxUpload = function (data, requestUrl, mimeType) {
        var deferred = Deferred();

        var xhr = SmartAjax.makeRequest({
            type: 'POST',
            url: requestUrl,
            contentType: mimeType,
            processData: false,
            data: data,
            timeout: exports.REQUEST_TIMEOUT_MILLIS,
            success: deferred.resolve.bind(deferred),
            error: deferred.reject.bind(deferred),
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();

                deferred.notify('init');
                xhr.upload.addEventListener("progress", deferred.notify.bind(deferred));
                return xhr;
            }
        });

        deferred.always(function () {
            $.ajaxSettings.xhr().removeEventListener("progress", exports.progressView.progressHandler);
            $(DialogRegister.attachScreenshotDialog).off("Dialog.hide", xhr.abort);
        });

        // cancel upload on dialog hide
        $(DialogRegister.attachScreenshotDialog).one("Dialog.hide", xhr.abort);

        return deferred.promise();
    };

    /**
     * Trigger Attach Screenshot dialog and resolve deferred once it is open
     *
     * @returns {$.Deferred}
     */
    exports.show = function () {
        var deferred = new Deferred();
        var $dialogTrigger = $(".issueaction-attach-screenshot-html5");

        // Such situation is not supported yet
        // TODO remove once we add support for textfields in create issue dialog (temporary attachments problem)
        if (Dialog.current != null) {
            return deferred.reject().promise();
        }

        // trigger is not available
        // TODO refactor when we start supporting create issue dialog
        if ($dialogTrigger.length == 0) {
            return deferred.reject().promise();
        }

        // IE11 quirk: if this is called within onPaste handler, browser will throw "Access denied" on any XHR
        _.defer(function () {
            $dialogTrigger.trigger("click");
        });

        $(document).on("dialogContentReady", function (event, dialog) {
            if (dialog === DialogRegister.attachScreenshotDialog &&
              exports.dialogView.isEnabled()) {
                deferred.resolve(dialog);
            } else {
                deferred.reject();
            }
        });

        // each impression of dialog causes ajax request, reject deferred if there was request for dialog content, but after some brief time deferred was still not resolved
        $(document).on("ajaxComplete.jira.screenshot.dialog", function (event, jqXhr, options) {
            if (options.url.indexOf($dialogTrigger.attr("href")) > -1) {
                setTimeout(function () {
                    if (!deferred.state() === "resolved") {
                        deferred.reject();
                    }
                }, 1000);
            }
        });

        // unbind from ajaxComplete
        deferred.always(function () {
            $(document).off("ajaxComplete.jira.screenshot.dialog");
        });

        return deferred.promise();
    };
});
