package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

public class CreateIssueDialogDropZone extends FormDropZone {

    @ElementBy(cssSelector = ".issue-drop-zone[duiType='dndattachment/dropzones/CreateIssueDropZone'].-dui-type-parsed")
    private PageElement dropZoneElement;

    @ElementBy(cssSelector = "#create-issue-dialog")
    private PageElement attachFileElement;

    public PageElement getDropZoneElement() {
        return dropZoneElement;
    }

    public By getProgressBarLocator() {
        return By.cssSelector(".issue-drop-zone[duiType='dndattachment/dropzones/CreateIssueDropZone']~.upload-progress-bar");
    }

    public PageElement getContextElement() {
        return attachFileElement;
    }
}
