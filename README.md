## Branches
- master - JIRA 8.x
- jddap_4_4_x - JIRA 7.3+
- jddap_4_3_x - JIRA 7.2
- jddap_4_2_x - JIRA 7.1
- jddap_4_1_x - JIRA 7.0
- jddap_3_3_x - JIRA 6.5
- jddap_3_2_x - JIRA 6.4
- jddap_3_1_x - JIRA 6.1 - 6.3

## How to contribute

1. Raise a ticket in https://ecosystem.atlassian.net/browse/JDDAP
2. Branch from master, name pattern form branches: issue/{ISSUE_KEY}-shorthand-name
3. Test your changes, make sure that CI for your branch is green
4. Create pull request
5. Once PR is accepted and merged, proceed to release and deployment

## How to run/debug tests (IntelliJ)

1. cd plugin
2. mvn jira:debug
3. Create run configuration in IntelliJ for i.e. TestDialogs and set $MODULE_DIR$ as Working directory
4. Run/debug test

## How to run tests from command line

1. mvn clean integration-test -DintegrationTests -Dxvfb.enable=false

## How to run single test from command line

1. cd plugin
2. mvn jira:debug
3. cd ../tests/integration-tests
4. mvn -Dtest=TestDialogs test

## How to run tests against JIRA snapshot

1. cd plugin
2. mvn jira:debug -Djira.version=6.4-SNAPSHOT
3. cd ../tests/integration-tests
4. mvn -Dtest=TestDialogs test

## How to release

- Use release plan on JBAC in JDDAP project

## Where are the builds?
JBAC: https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINS-JDDAP

## How to deploy to cloud

- Use manifesto pipeline on JBAC in JDDAP project

## JIRA Drag & Drop Attachments Plugin ##

Copyright (c) 2013, Atlassian Pty Ltd
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
