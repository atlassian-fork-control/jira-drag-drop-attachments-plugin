define('dndattachment/dropzones/IssueDropZone', ['require'], function(require) {
    var formatter = require('jira/util/formatter');
    var logger = require('jira/util/logger');
    var analytics = require('jira/analytics');
    var SmartAjax = require('jira/ajs/ajax/smart-ajax');
    var IssueApi = require('jira/issue');
    var DirtyForm = require('jira/jquery/plugins/isdirty');
    var Meta = require('jira/util/data/meta');
    var Class = require('jira/lib/class');
    var _ = require('underscore');
    var Deferred = require('jira/jquery/deferred');
    var $ = require('jquery');
    var Flag = require('jira/flag');
    var Parser = require('dndattachment/Parser');
    var I18n = require('dndattachment/i18n');
    var DnDTemplates = require('dndattachment/templates');
    var DataTransfer = require('dndattachment/util/DataTransfer');

    var AUTO_DESTROY_DELAY = 5000;
    var INTERNAL_ERROR = [formatter.I18n.getText("dnd.attachment.internal.server.error")];

    var IssueDropZone = Class.extend({

        eventGroup: 'issuedropzone',

        progressBarType: 'dndattachment/progressbars/UploadProgressBar',

        init: function(element) {
            this.$node = $(element);

            if(!this.isSupportedBrowser()) {
                return this.renderUnsupported();
            }

            if(this.$node.parents('.mod-content')) {
                this.$node.parents('.mod-content').addClass('issue-drop-zone');
            }

            this.uploadLimit = this.$node.data('upload-limit');
            this.uploadSize = this.$node.data('upload-size');
            this.formToken = this.$node.data('form-token');
            this.attachFileUrl = contextPath + '/secure/AttachFile.jspa';

            this.pendingQueue = [];

            this.render();

            this.queueEvent('init');
        },

        queueTask: function(task) {
            this.pendingQueue.push(task);
            task.always(function() {
                this.pendingQueue.splice(this.pendingQueue.indexOf(task), 1);
            }.bind(this));

            this.markDirty(true);

            // debounce required to make transition -> commit possible
            $.when.apply(window, this.pendingQueue).always(_.debounce(this.checkMarkDirty, false, true).bind(this));
        },

        render: function() {
            this.$node.html(DnDTemplates.IssueDropZone({}));

            this.fileInput = this.$node.find('.issue-drop-zone__file');
            this.fileButton = this.$node.find('button.issue-drop-zone__button');

            this.connectListeners();

            logger.trace("jira.issue.dnd.issuedropzone.render");
        },

        connectListeners: function() {
            this.fileInput.change(function(event) {
                this.handleFilesReceived(this.fileInput[0].files);
                this.queueEvent('fileInput', { count: this.fileInput[0].files.length });
                this.fileInput.attr('value', null);
            }.bind(this));

            this.fileButton.click(function(e){
                e.preventDefault();
                this.fileInput.click();
            }.bind(this));

            var $dropNode = this.$node.parents('.issue-drop-zone').length > 0 ? this.$node.parents('.issue-drop-zone') : this.$node;
            this.$dropTarget = $dropNode.find('>*').andSelf();

            this.$dropTarget.on('dragover dragenter drop', function(event) {
                event.preventDefault();
                event.stopPropagation();

                $dropNode.addClass('dragover');

                if(event.type == 'drop') {
                    $dropNode.removeClass('dragover');
                    $(document).trigger("dropHandled");

                    var dataTransfer = new DataTransfer(event.dataTransfer);
                    dataTransfer.getFiles().then(function(files) {
                        this.handleFilesReceived(files);
                        this.queueEvent('fileDrop', { count: files.length });
                    }.bind(this));
                }
            }.bind(this));

            this.$dropTarget.on('dragleave', function() {
                $dropNode.removeClass('dragover');
            }.bind(this))
        },

        configureUploadProgressBar: function($progressBar, file) {
            $progressBar.data("upload-size", this.uploadSize).
                data("file", file).
                data("form-token", this.formToken);
        },

        /**
         * It will create progress bar for given file and place it in drop zone
         * @param {File} file An object that comes from input field or drop object
         * @returns {UploadProgressBar}
         */
        createUploadProgressBar: function(file) {
            var $progressBar = $('<div duiType="'+this.progressBarType+'" class="upload-progress-bar"></div>');
            this.configureUploadProgressBar($progressBar, file);

            return Parser.parse($progressBar).done(function(progressBar) {
                this.placeUploadProgressBar($progressBar, progressBar);
                this.handleNewProgressBar(progressBar);
            }.bind(this));
        },

        placeUploadProgressBar: function($progressBar) {
            return $progressBar.insertAfter(this.$node);
        },

        handleNewProgressBar: function(progressBar) {
            // This is to be overridden by parent classes if needed
        },

        // The default behaviour for handling files is to upload them
        handleFilesReceived: function (files) {
            this.uploadFiles(files);
        },

        uploadFiles: function(files) {

            var uploadFilesResult = new Deferred();
            var progressBars = [];
            var issueKey = Meta.get("issue-key");
            var issueLink = Meta.get("viewissue-permlink");
            // user could be editing the summary field || not editing
            var issueSummary = $("#summary-val input").attr("value") || $("#summary-val").text();
            var issueLinkHTML = DnDTemplates.SuccessMessageLink({ issueLink: issueLink, issueKey: issueKey, issueSummary: issueSummary });
            var queue = _.map(files, function(file) {
                var result = new Deferred();

                this.createUploadProgressBar(file).then(function(uploadProgressBar) {
                    this.trigger('progressBarInserted', uploadProgressBar);
                    progressBars.push(uploadProgressBar);
                    uploadProgressBar.uploadFile(file, this.uploadLimit)
                        .then(result.resolve.bind(result), result.resolve.bind(result))
                        .done(function() {
                            this.trigger('uploadFinished', uploadProgressBar);
                        }.bind(this))
                        .fail(function(){
                            uploadFilesResult.reject(arguments);
                        });
                }.bind(this));

                return result;
            }.bind(this));

            var uploadQueue = $.when.apply(window, queue);

            uploadQueue.done(function() {
                var fileIDs = _.filter(queue.length == 1 ? [arguments] : arguments, function(result) {
                    return result && result[0] && result[0].id }).map(function(result) {
                    return result[0].id
                });

                if(fileIDs.length == 0) {
                    return;
                }

                this.commitUpload(fileIDs).done(function() {
                    // auto destroy progress bars
                    _.delay(function() {
                        progressBars.some(function(progressBar) {
                            if(progressBar.getAutoDestroy()) {
                                progressBar.destroy();
                                this.queueEvent('autoDestroy')
                            }
                        }.bind(this));
                    }.bind(this), AUTO_DESTROY_DELAY);

                    var successfulUploads = _.filter(progressBars, function(progressBar){
                        return progressBar.result.state() === "resolved";
                    });

                    if(successfulUploads.length == 1){
                        var fileName = successfulUploads[0].getFileName();
                        Flag.showSuccessMsg("", formatter.I18n.getText("dnd.attachment.successful.upload.one", fileName, issueLinkHTML));
                    }
                    else if(successfulUploads.length > 1){
                        Flag.showSuccessMsg("", formatter.I18n.getText(
                            "dnd.attachment.successful.upload.many",
                            successfulUploads.length,
                            issueLinkHTML
                        ));
                    }
                    uploadFilesResult.resolve(
                        _.map(successfulUploads, function(successfulUpload) {
                            return successfulUpload.file.name;
                        })
                    );
                }.bind(this)).fail(function(errorMessages, errors) {
                    uploadFilesResult.reject(errorMessages, errors);
                    if(errorMessages) {
                        errorMessages.forEach(function(errorMessage) {
                            progressBars.forEach(function(progressBar) {
                                progressBar.reportError(errorMessage);
                            });
                        });
                    }

                    if(errors) {
                        progressBars.forEach(function(progressBar) {
                            var errorMessage = errors['#' + progressBar.getFileID()] || errors[progressBar.getFileName()];
                            if(errorMessage) {
                                progressBar.reportError(errorMessage);
                            }
                        });
                    }

                    logger.trace('jira.issue.dnd.commit.fail');
                });
            }.bind(this));

            this.queueTask(uploadQueue);

            return uploadFilesResult;
        },

        /**
         *
         * @param attachments
         */
        loadAttachments: function(attachments) {
            attachments.forEach(function(attachment) {
                this.createUploadProgressBar(attachment.file).then(function(progressBar) {
                    progressBar.setFileID(attachment.id);
                    progressBar.setFileName(attachment.name);
                    progressBar.setFileSize(attachment.size);
                    progressBar.setProgress(1);
                    progressBar.setFinished();

                    this.trigger('uploadFinished', progressBar);
                }.bind(this));
            }, this);
        },

        commitUpload: function(fileIDs) {
            // By default, do nothing. This method is typically overridden
            return new Deferred();
        },

        attachFile: function(fileIDs) {
            var result = new Deferred();

            SmartAjax.makeRequest({
                type: "POST",
                url: this.attachFileUrl,
                data: {
                    inline: true,
                    id: IssueApi.getIssueId(),
                    filetoconvert: fileIDs,
                    atl_token: atl_token(),
                    formToken: this.formToken
                },
                dataType: "json"
            })
            .done(result.resolve.bind(result))
            .fail(function(jqXHR) {
                try {
                    var response = JSON.parse(jqXHR.responseText);
                    result.reject(response.errorMessages || INTERNAL_ERROR, response.errors);
                } catch(ex) {
                    result.reject(INTERNAL_ERROR);
                }
            });

            return result;
        },

        isSupportedBrowser: function() {
            if ($('html.webkit')[0] || $('html.safari')[0]) {
                if ($.browser.chrome && $.browser.versionNumber < 49) {
                    return false;
                } else if ($.browser.safari && $.browser.versionNumber < 10) {
                    return false;
                } else if ($.browser.opera && $.browser.versionNumber < 35) {
                    return false;
                }
            } else {
                if ($('html.mozilla')[0]) {
                    if ($.browser.versionNumber < 42) {
                        return false;
                    }
                } else if ($('html.msie')[0]) {
                    if ($.browser.versionNumber < 10) {
                        return false;
                    }
                } else {
                    return false; // we don't support anything else than IE10, Mozilla or WebKit
                }
            }

            return true;
        },

        renderUnsupported: function() {
            this.$node.addClass('issue-drop-zone__not-supported').text(I18n("dnd.attachment.unsupported.browser")).append('<ul><li>FireFox 3.6+</li><li>Chrome 5+</li><li>Safari 5</li></ul>');
        },

        markDirty: function(isDirty) {
            // DirtyForm
            this.$node.addClass(DirtyForm.ClassNames.SANCTIONED);
            this.$node.toggleClass(DirtyForm.ClassNames.EXEMPT, !isDirty);
            this.$node.attr('value', false);
            this.$node.attr('defaultValue', true);
        },

        checkMarkDirty: function() {
            // all tasks are done, succesful or not
            if(this.pendingQueue.length == 0)
                this.markDirty(false);
        },

        /**
         * Analytics
         */

        queueEvent: function(name, props) {
            analytics.send({ name: 'issue.dnd.attachment.'+this.eventGroup+'.'+name, data: props || {} });
        }

    });

    return IssueDropZone;
});
