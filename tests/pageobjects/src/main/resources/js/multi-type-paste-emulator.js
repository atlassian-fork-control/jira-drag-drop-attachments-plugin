// Paste event emulator
// this code will be executed inside function
// so ignore IntelliJ warning about arguments
var element = arguments[0];
var fileName = arguments[1];
var evt = jQuery.Event( "paste" );
var rtfBlob = new Blob(["{\rtf1\ansi{\fonttbl\f0\fswiss Helvetica;}\f0\pard This is some {\b bold} text. \par}"], {type: 'text/rtf'});
evt.clipboardData = {
    files: [
        document.createElement('canvas').mozGetAsFile(fileName),
        rtfBlob
    ]
};
element.focus();
setTimeout(function() {
    jQuery(element).trigger(evt);
});
