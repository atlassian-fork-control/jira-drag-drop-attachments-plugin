define("dndattachment/ctrlv/initialize", ['require', 'exports'], function(require, exports) {
  "use strict";

  var Deferred = require('jira/jquery/deferred');
  var $ = require('jquery');
  var _ = require('underscore');
  var utility = require('dndattachment/ctrlv/utility');
  var html5 = require('dndattachment/ctrlv/html5');
  var issue_paste = require('dndattachment/ctrlv/issue-paste');
  var tracker = require('dndattachment/ctrlv/tracking');
  var executor = require('dndattachment/ctrlv/executor');
  var DialogRegister = require('jira/dialog/dialog-register');
  var FormDialog = require('jira/dialog/form-dialog');

  // Initialization of HTML5 paste handler
  exports.init = function() {
    var AttachImageDialog = FormDialog.extend({
      options: {}
    });

    /**
     * SW-306 js errors are present when this reference isn't available in global scope
     * @deprecated
     */
    window.JIRA.ScreenshotDialog = AttachImageDialog;

    issue_paste.initIssuePaste();

    // Upload and Cancel Button Handlers
    $(document).ready(function () {
      "use strict";

      var delayShowUntil = Deferred();

      delayShowUntil.resolve();

      // Shows the Attach Screenshot in a Popup
      DialogRegister.attachScreenshotDialog = new AttachImageDialog({
        id: "attach-screenshot-dialog",
        trigger: ".issueaction-attach-screenshot-html5",
        isIssueDialog: true,
        onContentRefresh: function attachScreenshotContentRefresh() {

          this.$form.bind("before-submit", function attachScreenshotContentRefreshBeforeSubmit(e) {
            var errors = html5.validateFormData(html5.screenshotFileUpload, $.trim(html5.dialogView.getFileNameInput().val()));
            if (html5.dialogView.getFileSize() == 0) {
              e.preventDefault();
              return false;
            } else if (!_.isEmpty(errors)) {
              html5.dialogView.displayErrors(errors);
              e.preventDefault();
              return false;
            }
            return true;
          });

        },
        delayShowUntil: function () {
          return delayShowUntil;
        }
      });

      $(document).bind("dialogContentReady", function (event, dialog) {
        // Ensure that the dialog is *ours*, and that it's showing the Attach screen shot form. It's possible that
        // it's populated with an error from the server, in which case we wouldn't want to "init" it.
        if (dialog === DialogRegister.attachScreenshotDialog && document.getElementById("attach-screenshot-form") !== null) {
          tracker.trigger("attach.screenshot.html5.contentReady");
          html5.initScreenshotPasteHandler();
        }
      });

      $(document).ready(function () {
        // Atlassian Analytics - Capture click events
        $(document).on("click", "#attach-screenshot-html5", function () {
          tracker.trigger("attach.screenshot.html5.display");
        });
      });
    });

    executor.register();
  };
});

require([
  "dndattachment/ctrlv/initialize",
  "jquery"
], function(
  jhtml5,
  $
) {
  "use strict";
  $(function() { jhtml5.init(); });
});
