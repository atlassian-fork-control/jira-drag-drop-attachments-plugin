package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.framework.util.JiraLocators;
import com.atlassian.jira.pageobjects.navigator.AdvancedSearch;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsDropZone;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsSection;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.DropZone;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import com.google.common.io.Files;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;
import java.util.regex.Pattern;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest(WEBDRIVER_TEST)
@Category(OnDemandAcceptanceTest.class)
public class TestIssueDropZoneBase extends BaseWebdriverTest
{
    @Inject
    private TraceContext traceContext;

    @Inject
    private ProductInstance jiraProduct;

    @Inject
    private PageElementFinder elementFinder;

    @Test
    public void testViewIssues() throws Exception
    {
        AdvancedSearch search = JIRA.goToIssueNavigator().enterQuery(String.format("summary ~ \"%s\"", ISSUE_SUMMARY));
        Tracer tracer = traceContext.checkpoint();
        search.submit();
        traceContext.waitFor(tracer, "AJS.$.ajaxComplete", Pattern.compile(jiraProduct.getContextPath() + "/rest/analytics/1.0/publish/bulk"));

        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());

        int attachmentCount = ((List<?>) backdoor.issues().getIssue(issueKey).fields.get("attachment")).size();

        tracer = traceContext.checkpoint();

        DropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME, tracer);

        assertFalse("Body does not have dragover class", elementFinder.find(JiraLocators.body()).hasClass(DropZone.DRAGOVER_CLASS));

        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

        if (attachmentCount == 0)
        {
            assertTrue("attachment section became visible", pageBinder.bind(AttachmentsSection.class).isVisible());
        }

        search = JIRA.goToIssueNavigator().enterQuery(String.format("summary ~ \"%s\"", ISSUE_SUMMARY));
        tracer = traceContext.checkpoint();
        search.submit();
        traceContext.waitFor(tracer, "AJS.$.ajaxComplete", Pattern.compile(jiraProduct.getContextPath() + "/rest/analytics/1.0/publish/bulk"));

        int fileCount = pageBinder.bind(AttachmentsSection.class).getFileCount();
        assertEquals("One image on upload list", 1, fileCount - attachmentCount);
    }

    @Test
    public void testAttachments()
    {
        JIRA.goToViewIssue(issueKey);

        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());

        int attachmentCount = ((List<?>) backdoor.issues().getIssue(issueKey).fields.get("attachment")).size();

        Tracer tracer = traceContext.checkpoint();

        DropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME, tracer);

        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

        if (attachmentCount == 0)
        {
            assertTrue("attachment section became visible", pageBinder.bind(AttachmentsSection.class).isVisible());
        }

        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("Attachments count increased by one", 1, fileNames.size() - attachmentCount);
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", fileNames.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", fileNames.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }
}
