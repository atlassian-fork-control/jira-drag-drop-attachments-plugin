// Paste event emulator
// this code will be executed inside function
// so ignore IntelliJ warning about arguments
var element = arguments[0];
var fileName = arguments[1];
var evt = jQuery.Event( "paste" );
evt.clipboardData = {files: [ document.createElement('canvas').mozGetAsFile(fileName) ]};
element.focus();
setTimeout(function() {
    jQuery(element).trigger(evt)
});
