package it.com.atlassian;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import org.openqa.selenium.Dimension;

/**
 * AUI-related utility methods for use during WebDriver tests.
 *
 * @since 4.4.14
 */
public final class AuiUtils {

    /**
     * Closes all AUI flags to work around the problem whereby they
     * prevent WebDriver interacting with important UI elements.
     *
     * @param jira the Jira instance under test
     */
    public static void closeAllAuiFlags(final JiraTestedProduct jira) {
        jira.getTester().getDriver().manage().window().setSize(new Dimension(1440, 900));
        jira.getPageBinder().bind(AUIFlags.class).closeAllFlags();
    }

    private AuiUtils() {}
}
