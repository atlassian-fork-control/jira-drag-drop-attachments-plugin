package com.atlassian.jira.plugins.dnd.attachment.providers;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import webwork.config.Configuration;

public class UploadLimitProvider implements WebResourceDataProvider
{
    @Override
    public Jsonable get()
    {
        return new JsonableString(Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE));
    }
}
