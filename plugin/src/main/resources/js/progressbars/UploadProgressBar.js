define('dndattachment/progressbars/UploadProgressBar', ['require'], function(require) {
    var analytics = require('jira/analytics');
    var formatter = require('jira/util/formatter');
    var logger = require('jira/util/logger');
    var IssueApi = require('jira/issue');
    var InlineAttach = require('jira/attachment/inline-attach');
    var Class = require('jira/lib/class');
    var _ = require('underscore');
    var wrmContextPath = require('wrm/context-path');
    var Deferred = require('jira/jquery/deferred');
    var $ = require('jquery');
    var Parser = require('dndattachment/Parser');
    var I18n = require('dndattachment/i18n');
    var Attachments = require('dndattachment/TemporaryAttachments');
    var DnDTemplates = require('dndattachment/templates');
    var Config = require('dndattachment/util/Configuration');
    var FileSizeUtil = require('dndattachment/util/FileSizeUtil');

    var ICONS = ['aui-iconfont-error', 'aui-iconfont-success', 'aui-iconfont-close-dialog'];

    function queueEvent(name, props) {
        analytics.send({ name: 'issue.dnd.attachment.uploadprogress.'+name, data: props || {} });
    }

    var thumbnailMimeTypes = Config.getWRM("thumbnail-mime-types").split(",");

    var UploadProgressBar = Class.extend({

        autoDestroy: true,

        init: function(element) {
            this.result = new Deferred();
            this.$node = $(element);
            this.file = this.$node.data("file");
            this.uploadSize = this.$node.data('upload-size');
            this.formToken = this.$node.data('form-token');

            this.$node.prop("_instance", this);

            this.render();
        },

        render: function() {
            this.$node.html(DnDTemplates.UploadProgressBar({isImageType: this.isImageType()}));

            this.afterRender();
        },

        afterRender: function() {
            if (this.file) {
                this.setFileName(this.file.name);
                this.setFileSize(this.file.size);
                this.loadThumbnail(this.file);
            }

            if (this.progress)
            {
                this.updateProgress();
            }

            this.connectListeners();
        },

        connectListeners: function() {
            this.getControlNode().on('click', function() {
                this.destroy();
                queueEvent('buttonClick');
            }.bind(this));
        },

        destroy: function () {
            if (this.destroyed) {
                return;
            }

            if(this.upload) {
                this.upload.abort();
            }

            this.setAutoDestroy(false);
            this.trigger('onBeforeDestroy');
            this.destroyed = true;
            this.$node.animate({ opacity: 0 }, {
                duration: 250,
                complete: function () {
                    this.$node.slideUp(250, function () {
                        if (this.objectURL)
                            window.URL.revokeObjectURL(this.objectURL);
                        this.$node.remove();
                        logger.trace('jira.issue.dnd.progressbar.removed');

                        this.trigger('onDestroy');
                    }.bind(this));
                }.bind(this)
            });
        },

        isDestroyed: function() {
            return !!this.destroyed;
        },

        setAutoDestroy: function(enabled) {
            this.autoDestroy = enabled;
        },

        getAutoDestroy: function() {
            return this.autoDestroy;
        },

        getUploadParams: function(file) {
            var uploadParams = {
                filename: file.name,
                size: file.size,
                atl_token: atl_token(),
                formToken: this.formToken
            };

            _.extend(uploadParams, this.getEntityParams());

            return uploadParams
        },

        /**
         * Return object with projectKey, projectId, issueKey and/or issueId
         */
        getEntityParams: function() {
            var entityParams = {};

            var issueId = IssueApi.getIssueId();
            var projectId = this.$node.parents('form').find('*[name="pid"]').attr('value');

            // projectId has priority over issueId
            if(projectId) {
                entityParams.projectId = projectId;
            } else if(issueId) {
                entityParams.issueId = issueId;
            }

            return entityParams;
        },

        setFile: function(file) {
            this.file = file;
        },

        setFileID: function(id) {
            this.fileID = id;
            this.$node.attr("data-file-id", id);
        },

        getFileID: function() {
            return this.fileID;
        },

        setFileName: function(fileName) {
            this.$node.find('.upload-progress-bar__file-name').text(fileName);
        },

        getFileName: function() {
            return this.$node.find('.upload-progress-bar__file-name').text();
        },

        setFileSize: function(fileSize) {
            if(fileSize >= 0) {
                this.$node.find('.upload-progress-bar__file-size').html(FileSizeUtil.format(fileSize));
            }
        },

        uploadFile: function(file, uploadLimit) {
            this.setFile(file);

            var result = this.result;

            this.monitorUpload(result);
            result.fail(this.reportError.bind(this));

            if(file.size > uploadLimit) {
                result.reject(I18n("dnd.attachment.file.is.too.large").replace('{0}', this.uploadSize), true);
                return result;
            }

            var invalidChars = [ '\\', '/','"', ':','?', '*', '<','|','>' ];
            for (var i = 0; i < invalidChars.length; i++) {
                var invalidChar = invalidChars[i];
                if (_.contains(file.name, invalidChar)) {
                    result.reject(formatter.I18n.getText("dnd.attachment.invalidcharacter", file.name, invalidChar), true);
                    return result;
                }
            }

            this.upload = new InlineAttach.AjaxUpload({
                file: file,
                params: this.getUploadParams(file),
                url: InlineAttach.AjaxPresenter.DEFAULT_URL,
                progress: function(val) {
                    result.notify(val / file.size);
                },
                success: function(val, status) {
                    if(val.id !== undefined && val.name !== undefined) {
                        result.notify(1);
                        result.resolve(val, status, file);
                    } else
                        result.reject(val, status, file);
                },
                error: function(text, status) {
                    result.reject(I18n("dnd.attachment.internal.server.error"), status);
                },
                abort: function() {
                    result.reject(I18n("dnd.attachment.upload.aborted"), "abort");
                }
            });

            // check if current session is able to create attachments
            this.checkSession().then(function() {
                this.upload.upload();
            }.bind(this)).fail(function() {
                result.reject(I18n("dnd.attachment.unauthorized"), 401);
            });

            this.$node.find('.upload-progress-bar__control>*').on('click', function() {
                this.upload.abort();
                result.reject();
            }.bind(this));

            result.done(function(val) {
                Attachments.putAttachment(val.id, this.file, { name: val.name });

                this.setFile(this.file);
                this.setFileName(val.name);
                this.setFileID(val.id);
                logger.trace('jira.issue.dnd.uploaded');
            }.bind(this));

            return result;
        },

        monitorUpload: function(promise) {
            promise.progress(this.setProgress.bind(this));

            promise.done(this.setFinished.bind(this));

            promise.fail(this.setFailed.bind(this));
        },

        setProgress: function(state) {
            this.progress = state;

            this.updateProgress();
        },

        updateProgress: function() {
            var progressBar = this.$node.find('.upload-progress-bar__progress-bar');
            progressBar.toggleClass('UploadProgressBar_progressUnknown', this.progress === false);
            if(this.progress >= 0) {
                progressBar.find('.upload-progress-bar__bar').css('width', 100 * this.progress + '%');
            }
        },

        setFinished: function() {
            this.$node.addClass('upload-progress-bar__upload-finished');
            this.$node.find('.upload-progress-bar__control button .aui-icon')
                .addClass('aui-iconfont-success')
                .removeClass('aui-iconfont-close-dialog');
            this.finished = true;
            this.trigger("onFinished");
        },

        setFailed: function() {
            this.$node.addClass('UploadProgressBar_uploadFailed');
            this.finished = true;
            this.failed = true;
            this.trigger("onFailed");
        },

        isFinished: function() {
            return !!this.finished;
        },

        isFailed: function() {
            return !!this.failed;
        },

        isSuccessful: function() {
            return this.isFinished() && !this.isFailed();
        },

        isImageType: function() {
            return this.file && this.file.type && _.contains(thumbnailMimeTypes, this.file.type);
        },

        getThumbnailSrc: function(file) {
            if(this.isImageType()) {
                this.objectURL = window.URL.createObjectURL(file);
            }

            return this.objectURL || JIRA.Templates.ViewIssue.matchFileIconUrl({ baseurl: wrmContextPath(), mimetype: file.type });
        },

        getThumbnailNode: function ()
        {
            return this.$node.find('.upload-progress-bar__thumbnail');
        },

        getControlNode: function () {
            return this.$node.find('.upload-progress-bar__control button');
        },

        loadThumbnail: function(file) {
            var imgSrc = this.getThumbnailSrc(file);

            var $thumbnailNode = this.getThumbnailNode();
            $thumbnailNode.addClass(this.objectURL ? 'upload-progress-bar__thumbnail_image' : 'upload-progress-bar__thumbnail_icon');

            var $thumbnail = $('<img/>').attr('src', imgSrc);

            return $thumbnail.appendTo($thumbnailNode);
        },

        reportError: function(message, status) {
            if(typeof message == "object" && message.errorMessage)
                message = message.errorMessage;

            this.$node.addClass('upload-progress-bar__upload-error');
            this.$node.removeClass('upload-progress-bar__upload-finished');
            this.$node.find('.upload-progress-bar__bar').css('width', '100%');
            this.showErrorMessage(message, status);
            this.setAutoDestroy(false);

            queueEvent('error', { message: message });
        },

        showErrorMessage: function(message) {
            this.$node.find('.upload-progress-bar__error-message').text(message);
        },

        setIcon: function(iconName) {
            var $icon = this.$node.find('.upload-progress-bar__control button .aui-icon');
            if(!ICONS.some(function(icon) {
                $icon.toggleClass(icon, icon == iconName);
                return icon == iconName;
            })) {
                throw "Unknown icon";
            }
        },

        checkSession: function() {
            var result = new Deferred();

            // auth session should just return 200, when user is logged in
            $.ajax({
                type: "GET",
                url: contextPath + "/rest/auth/1/session",
                contentType: "application/json"
            }).done(function() {
                // user is authorized
                result.resolve();
            }).fail(function() {
                // in case of user being not logged in, check explictly for CREATE_ATTACHMENT permissions
                this.checkPermissions().done(function(response) {
                    result.resolve();
                }).fail(function() {
                    result.reject();
                });
            }.bind(this));

            return result;
        },

        checkPermissions: function() {
            var result = new Deferred();

            $.ajax({
                type: "GET",
                url: contextPath + "/rest/api/2/mypermissions",
                contentType: "application/json",
                data: this.getEntityParams()
            }).done(function(response) {
                if(response.permissions["CREATE_ATTACHMENT"].havePermission) {
                    result.resolve();
                } else {
                    result.reject();
                }
            }).fail(function() {
                result.reject();
            });

            return result;
        }
    });

    return UploadProgressBar;
});
