define("dndattachment/ctrlv/utility", ['require', 'exports'], function(require, exports) {
    "use strict";

    var Deferred = require('jira/jquery/deferred');
    var $ = require('jquery');
    var _ = require('underscore');
    var base64decode = require('dndattachment/ctrlv/base64decode');
    var time = require('dndattachment/ctrlv/time');
    var version = require('dndattachment/ctrlv/version');
    var JIRAFlag = require('jira/flag');
    var Navigator = require('jira/util/navigator');

    /**
     * Determine if a keypress event is a "paste" (the keys differ between platform).
     * @param {Event} event
     * @param {string} [platform] If provided, overrides the value of navigator.platform.
     * @returns {boolean}
     */
    exports.isKeyPasteEvent = function (event, platform) {
        var V_KEY = 86;
        var IS_MAC = ((platform || "").indexOf("Mac") !== -1) || Navigator.isMac();

        return event.which === V_KEY && (IS_MAC ? event.metaKey : event.ctrlKey);
    };

    /**
     * Feature detection, check whether given parameter is a valid clipboard event
     *
     * @param event JQuery Event object
     * @returns {boolean}
     */
    exports.isImagePasteEvent = function (event) {
        if (event && event.clipboardData) {
            // Get the items from the clipboard, scan items and files properties
            var items = _.union(_.toArray(event.clipboardData.items), _.toArray(event.clipboardData.files));
            var hasImage = items.some(function (item) {
                return item.type.indexOf("image") !== -1;
            });
            var hasRtf = items.some(function (item) {
                return item.type.indexOf("rtf") !== -1;
            });
            return hasImage && !hasRtf;
        }
    };

    var getHtmlImagePaste = function (event) {
        if (!event.clipboardData || !event.clipboardData.types || !_.contains(event.clipboardData.types, "text/html")) {
            return;
        }

        var $data = $(event.clipboardData.getData("text/html"));
        if($data.length === 1 && $data.children().length === 0 && $data[0].nodeName.toLowerCase() === "img"){
            return $data[0].src;
        }
        return null;
    };

    exports.getHtmlImagePaste = getHtmlImagePaste;

    /**
     * Detect whether given event comes with html content in its ClipboardData, that contains single image tag
     * @param {Event} event
     * @returns {Boolean}
     */
    exports.isHtmlImagePasteEvent = function(event) {
        return getHtmlImagePaste(event) != null;
    };

    /**
     * Detect whether given event comes with text content in its ClipboardData
     * @param {Event} event
     * @returns {Boolean}
     */
    exports.isTextPasteEvent = function(event) {
        return event.clipboardData.types && (_.contains(event.clipboardData.types, "text/plain") || _.contains(event.clipboardData.types, "text/html"));
    };

    /**
     * Detect whether this element is an/withing contentEditable
     * @param {HTMLElement} element
     * @returns {Boolean}
     */
    exports.isContentEditable = function(element) {
        return  element.contentEditable == "true" || element.contentEditable == "" ||
                (element.contentEditable == "inherit" && $(element).parents().is('[contenteditable=true]'));
    };

    /**
     * Check if provided file name does not contain any of forbidden characters
     * @param {String} fileName
     * @returns {boolean}
     */
    exports.isValidFileName = function(fileName) {
        return !_([
            // characters forbidden by various file systems
            '\\', '/', '\"', ':', '?', '*', '<', '|', '>',
            // wiki markup
            '!'
        ]).any(function(character) {
            return fileName.indexOf(character) > -1;
        });
    };

    /**
     * Get text content from paste event
     * @param {Event} event
     * @returns {String}
     */
    exports.getTextPasteContent = function(event) {
        return event.clipboardData.types && _.contains(event.clipboardData.types, "text/plain") && event.clipboardData.getData("text/plain") || '';
    };

    /**
     * Get text contet from node, pick property which preserves new lines.
     * @param node
     * @returns {String}
     */
    exports.getTextContent = function(node) {
        if (!node) {
            return '';
        } else if (Navigator.isIE() && typeof node.innerText !== "undefined") {
            return node.innerText;
        } else  {
            return $(node).text();
        }
    };

    /**
     * Copy clipboardData from pasteEvent.originalEvent into pasteEvent or create and adapter
     * @param {Event} pasteEvent
     * @returns {Event}
     */
    exports.normalizePasteEvent = function(pasteEvent) {
        if(pasteEvent && pasteEvent.originalEvent && pasteEvent.originalEvent.clipboardData) {
            pasteEvent.clipboardData = pasteEvent.originalEvent.clipboardData;
        }

        // IE case, create small adapter for IE window.clipboardData
        if(pasteEvent && !pasteEvent.clipboardData && window.clipboardData) {
            pasteEvent.clipboardData = {
                files: window.clipboardData.files,
                types: {
                    contains: function(mimeType) {
                        if(mimeType == "text/plain") {
                            return !!window.clipboardData.getData("Text");
                        }
                    }
                },
                getData: function(mimeType) {
                    if(mimeType == "text/plain") {
                        return window.clipboardData.getData("Text");
                    }
                }
            };
        }

        return pasteEvent;
    };

    /**
     * Determine if the current browser is supported.
     * @param {string} [platform] If provided, overrides the value of navigator.platform.
     * @returns {boolean}
     */
    exports.browserIsSupported = function (platform) {
        var isMac = ((platform || "").indexOf("Mac") !== -1) || Navigator.isMac();
        var isSafari = Navigator.isSafari();

        // Safari isn't supported because it supports neither the HTML5 approach nor the Java applet approach.
        return !(isMac && isSafari) && !(version.isIE8() || version.isIE9() || version.isIE10());
    };

    /**
     * Determine wheter the current browser supports paste natively.
     * @returns {boolean}
     */
    exports.browserIsNativePaste = function () {
        return Navigator.isChrome() || Navigator.isSafari();
    };

    /**
     * Determine whether $pasteTarget is an input element and that it is a wiki textfield
     * @returns {boolean}
     */
    exports.isWikiTextfield = function ($pasteTarget) {
        return $pasteTarget.is(':input') && $pasteTarget.hasClass("wiki-textfield")
    };

    /**
     * Determine whether the element is the summary input field
     * @returns {boolean}
     */
    var _isSummaryField = function ($pasteTarget) {
        return $pasteTarget.is('input#summary');
    };

    /**
     * Append the given text content (if not null) to either the summary field or to a wiki textfield
     * @param content to be appended
     * @param pasteTarget element that will have the value updated
     * @param selectionStart
     * @param selectionEnd
     */
    var insertToInput = function (content, pasteTarget, selectionStart, selectionEnd) {
        var $pasteTarget = $(pasteTarget);

        // bail if pasteTarget is not a valid field or if no content was passed in
        if( !(exports.isWikiTextfield($pasteTarget) || _isSummaryField($pasteTarget)) || !content) {
            return;
        }

        if(!$pasteTarget.is(':focus')) {
            // This is a workaround for the case where the textfield is not focused (only needed for some browsers).
            // This ensures the cursor moves to the end of the text.
            $pasteTarget.one('focus', function() {
                var pasteTarget = $pasteTarget[0];
                pasteTarget.selectionStart = pasteTarget.selectionEnd = selectionStart + content.length;
            });
        }

        var value = $pasteTarget.val();
        var prefix = value.substring(0, selectionStart);
        var suffix = value.substring(selectionEnd, value.length);

        // wiki editor maintains its own history buffer, it helps us in some browser(Safari, IE)
        var wikiEditor = $pasteTarget.data("wikiEditor");
        if(wikiEditor && wikiEditor.undoRedoEl && _.isFunction(wikiEditor.undoRedoEl.recordHistoryItem)) {
            wikiEditor.undoRedoEl.recordHistoryItem();
        }

        $pasteTarget.val(prefix + content + suffix);

        // trigger input for dirty form handlers if there are any
        $pasteTarget.trigger("input");

        pasteTarget.selectionStart = pasteTarget.selectionEnd = selectionStart + content.length;

        if(wikiEditor && wikiEditor.undoRedoEl && _.isFunction(wikiEditor.undoRedoEl.updateCurrent)) {
            wikiEditor.undoRedoEl.updateCurrent();
        }
    };

    /**
     * Insert content into textfield at its current selection
     * @param {String} content
     * @param {HTMLElement} textfield
     * @param {Number} selectionStart current selection start
     * @param {Number} selectionEnd current selection end
     */
    exports.insertToInput = insertToInput;

    exports.isThumbnailsAllowed = function() {
        var thumbnailsAllowedMetadata = $("#dnd-metadata-webpanel").data("thumbnails-allowed");

        var canUseThumbnails = true;
        // metadata panel should always be present on every issue page, but just to be sure, default to true if not present
        if (thumbnailsAllowedMetadata != null) {
            // jquery should always be giving us back a boolean, as value should only ever be true/false, but just making sure its a boolean
            canUseThumbnails = !!thumbnailsAllowedMetadata;
        }

        return canUseThumbnails;
    };

    exports.getMarkup = function(fileName) {
        var notThumbnailableImageFileTypes = [
            'bmp',
            'gif'
        ];
        var regularImageFileTypes = [
            'pjpeg',
            'jpeg',
            'jpg',
            'png'
        ];
        var nameParts = fileName.split('.');
        var thumbnailsAllowed = exports.isThumbnailsAllowed();
        var fileType = nameParts[nameParts.length - 1].toLowerCase();

        if (thumbnailsAllowed && _.contains(regularImageFileTypes, fileType)) {
            return '!' + fileName + '|thumbnail!';
        } else if (_.contains(regularImageFileTypes, fileType) || _.contains(notThumbnailableImageFileTypes, fileType)) {
            return '!' + fileName + '!';
        } else {
            return '[^' + fileName + ']';
        }
    };

    /**
     * Wrap fileName into wiki markup and insert it into textfield at its current selection
     * @param {String} fileName
     * @param {HTMLElement} pasteTarget
     * @param {Number} selectionStart current selection start
     * @param {Number} selectionEnd current selection end
     * @param {Boolean} thumbnailsAllowed is whether instance allows thumbnails to be created or not
     */
    exports.insertWikiMarkup = function (fileName, pasteTarget, selectionStart, selectionEnd, thumbnailsAllowed) {
        var markup = exports.getMarkup(fileName, thumbnailsAllowed);
        if (markup) {
            // Add a space before and after so that it will display properly if the user forgets to add spaces
            markup = " " + markup + " ";
            insertToInput(markup, pasteTarget, selectionStart, selectionEnd);
        }
    };

    /**
     * Load image from given source
     * @param imageSrc
     * @returns {$.Deferred}
     */
    exports.loadImage = function (imageSrc) {
        var deferred = new Deferred();

        var image = new Image();
        image.setAttribute("crossOrigin", "anonymous");
        image.onload = function() {
            deferred.resolve(image);
        };
        image.onerror = deferred.reject.bind(deferred);
        image.src = imageSrc;

        // maybe it was already loaded (from cache)
        if(image.width > 0 && image.height > 0) {
            deferred.resolve(image);
        }

        return deferred;
    };

    /**
     * Convert image to blob or return null when it is not possible
     * @param image
     * @returns {Blob}
     */
    exports.convertImageToBlob = function(image) {
        // off screen canvas and try feature detection
        var canvas = $('<canvas>').attr("width", image.width).attr("height", image.height)[0];
        canvas.getContext('2d').drawImage(image, 0, 0);
        try {
            if(canvas.mozGetAsFile) {
                return canvas.mozGetAsFile("image/png");
            }

            if(canvas.toDataURL) {
                return new Blob([base64decode.decodeBase64DataUri(canvas.toDataURL("image/png"))], { type: "image/png" });
            }
        } catch(e) {
            // in case of any exceptions (security, malformed uri etc)
            return null;
        }
    };

    /**
     * Convert a blob into an image
     * @param blob
     * @param imageName
     * @return {File}
     */
    exports.convertBlobToImage = function(blob, imageName) {
        var file = new Blob([blob.slice()], { type: blob.type });
        file.lastModifiedDate = new Date();
        file.name = imageName;
        return file;
    };

    /**
     * Try to drop image file to element and return true if the event was actually consumed
     * @param {Blob} file
     * @param {jQuery} element
     * @returns {Boolean} Returns true if drop was actually consumed
     */
    exports.dropFileToElement = function(file, $element) {
        var fileName = file.name;
        if(!fileName) {
            fileName = this.generateFileName();
            file.name = fileName + ".png";
        }
        var event = $.Event("drop", {dataTransfer: {files: [file]}});

        var result = false;
        $(document).on('dropHandled.dropFileToElement', function() {
            result = true;
        });
        $element.trigger(event);
        $(document).off('.dropFileToElement');
        return result;
    };

    exports.generateFileName = function() {
        return "image-" + time().format("YYYY-MM-DD-HH-mm-ss-SSS");
    };

    exports.getCurrentIssueId = function() {
        return JIRA.Issues.Api.getSelectedIssueId();
    };

    exports.showErrorMsg = function(title, message){
        JIRAFlag.showErrorMsg(title, message);
    };

    /**
     * Create Blob from file with same mime type and copied file.name.
     * This is required, because we want to pass file with modified name in drop event and name property of File is read only
     * @param {File} file
     * @returns {Blob}
     */
    exports.createBlobFromFile = function(file) {
        var fileBlob = new Blob([file.slice()], { type: file.type });
        fileBlob.name = file.name;
        return fileBlob;
    };

    /**
     * Check whether a drag event contains files. To be used during dragover/dragenter events to determine whether
     * we should show the dropzone or not. If dataTransfer.types is not supported (only IE) just returns true.
     * This doesn't work completely on Firefox, since they count dragging an image element from within the browser
     * as a 'file'.
     * @param event
     * @returns {boolean}
     */
    exports.dragEventContainsFiles = function (event) {
        // If we can't check the types, assume it does contain files.
        if (!event.dataTransfer || !event.dataTransfer.types) {
            return true;
        }

        // Behaviour
        // Mozilla: types = DOMStringList and contains "Files" even when a file is not dragging. It contains "application/x-moz-file" when a file is actually dragging
        // IE: types = DOMStringList and contains "Files" when dragging  a file
        // Chrome + Safari: types = Array and contains "Files" when dragging a file

        var types = event.dataTransfer.types;

        if (Navigator.isMozilla()) {
            return _.contains(types, 'application/x-moz-file');
        } else {
            return _.contains(types, 'Files');
        }
    };
});
