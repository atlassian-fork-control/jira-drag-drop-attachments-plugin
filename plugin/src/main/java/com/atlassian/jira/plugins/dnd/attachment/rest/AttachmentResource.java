package com.atlassian.jira.plugins.dnd.attachment.rest;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachmentManager;
import com.atlassian.jira.issue.fields.rest.IssueFinder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.dto.AttachmentViewDtoConverter;
import com.atlassian.jira.issue.fields.rest.json.dto.AttachmentViewJsonDto;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;
import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

@Path ("attachment")
@AnonymousAllowed
@Produces ({ MediaType.APPLICATION_JSON })
public class AttachmentResource
{
    private final AttachmentService attachmentService;
    private final JiraAuthenticationContext authContext;
    private final IssueFinder issueFinder;
    private final I18nHelper i18nHelper;
    private final AttachmentManager attachmentManager;
    private final TemporaryWebAttachmentManager temporaryAttachmentManager;
    private final IssueUpdater issueUpdater;
    private final AttachmentViewDtoConverter attachmentConverter;

    public AttachmentResource(
            final AttachmentService attachmentService, final JiraAuthenticationContext authContext, final IssueFinder issueFinder, final I18nHelper i18nHelper, final AttachmentManager attachmentManager, final TemporaryWebAttachmentManager temporaryAttachmentManager, final IssueUpdater issueUpdater, final JiraBaseUrls jiraBaseUrls, final VelocityRequestContextFactory velocityRequestContextFactory, final AttachmentViewDtoConverter attachmentConverter)
    {
        this.attachmentService = attachmentService;
        this.authContext = authContext;
        this.issueFinder = issueFinder;
        this.i18nHelper = i18nHelper;
        this.attachmentManager = attachmentManager;
        this.temporaryAttachmentManager = temporaryAttachmentManager;
        this.issueUpdater = issueUpdater;
        this.attachmentConverter = attachmentConverter;
    }

    @POST
    @RequiresXsrfCheck
    public Response attachTemporaryfile(@FormParam ("id") String id, @FormParam ("formToken") String formToken, @FormParam ("filetoconvert") List<String> fileIdsToConvert)
    {
        JiraServiceContext context = new JiraServiceContextImpl(authContext.getLoggedInUser());
        Issue issue = getIssueObject(id);

        if (!attachmentService.canCreateAttachments(context, issue))
        {
            throw new RESTException(Response.Status.FORBIDDEN, ErrorCollection.of(i18nHelper.getText("attachment.service.error.create.no.permission")));
        }

        final AttachmentsBulkOperationResult<ChangeItemBean> convertResult = temporaryAttachmentManager.convertTemporaryAttachments(authContext.getUser(), issue, fileIdsToConvert);

        if (!convertResult.getErrors().isEmpty())
        {
            SimpleErrorCollection errors = new SimpleErrorCollection();
            for (AttachmentError error : convertResult.getErrors())
            {
                errors.addError(error.getFilename(), error.getLocalizedMessage(), error.getReason());
            }

            throw new RESTException(Response.Status.INTERNAL_SERVER_ERROR, ErrorCollection.of(errors));
        }

        final List<ChangeItemBean> changeItemBeans = convertResult.getResults();

        final IssueUpdateBean issueUpdateBean = new IssueUpdateBean(issue, issue, EventType.ISSUE_UPDATED_ID, authContext.getLoggedInUser());
        issueUpdateBean.setChangeItems(changeItemBeans);
        issueUpdateBean.setDispatchEvent(true);
        issueUpdateBean.setParams(ImmutableMap.<String, String>of("eventsource", "action"));

        issueUpdater.doUpdate(issueUpdateBean, true);

        // Build Attachment Json beans to send in the response.
        List<Attachment> attachments = new ArrayList<Attachment>();
        for (ChangeItemBean changeItemBean : changeItemBeans)
        {
            String idStringValue = changeItemBean.getTo();
            if (idStringValue != null)
            {
                Attachment attachment = attachmentManager.getAttachment(Long.valueOf(idStringValue));
                if (attachment != null)
                {
                    attachments.add(attachment);
                }
            }
        }
        Collection<AttachmentViewJsonDto> jsonDtos = attachmentConverter.convert(attachments);

        return Response.ok(jsonDtos).cacheControl(never()).build();
    }

    private MutableIssue getIssueObject(String issueIdOrKey) throws WebApplicationException
    {
        SimpleErrorCollection errors = new SimpleErrorCollection();
        MutableIssue issue = (MutableIssue) issueFinder.findIssue(issueIdOrKey, errors);
        if (issue == null)
        {
            throw new RESTException(Response.Status.NOT_FOUND, errors);
        }

        return issue;
    }

    static class RESTException extends WebApplicationException
    {
        RESTException(Response.Status status, SimpleErrorCollection errors)
        {
            super(Response.status(status).entity(ErrorCollection.of(errors)).cacheControl(never()).build());
        }

        RESTException(Response.Status status, ErrorCollection errors)
        {
            super(Response.status(status).entity(errors).cacheControl(never()).build());
        }
    }

}
